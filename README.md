# Lunchrocket

## About

This project is a simple CMS tailored to reviewing spots for lunch.

## Getting Started With Development

Because we are using Docker we don't need to worry about running the application
on our host computer, we just need to be able to run a docker container.

###### Docker, Docker Machine and Dinghy

We use docker for a virtual machine system. It means that this application will
run the same regardless of the host machine it's running on. It's also running
the exact same environment on our host computers as it would on the production
server. So, we save some hassle of environment changes.

```bash
brew install docker docker-machine docker-compose
```

We also use [Dinghy](https://github.com/codekitchen/dinghy). Which is a great
tool for development with docker, adds faster file volume mounting and some
virtual host tweaks.

As a depedancy for Dinghy you need Virtualbox and The Virtualbox extension pack.
https://www.virtualbox.org/wiki/Downloads

Once you have that.

```bash
brew tap codekitchen/dinghy
brew install dinghy
dinghy create --provider=virtualbox --memory=4096
```

For more information on Dinghy commands see the repo above.

Once that's finished up. We should have all we need to run a docker container.

###### Using Development Repo

Clone this repo.

Then cd into the directory and run

```bash
git submodule init
git submodule update
```

The frontend and backend repo is a submodule of the development repo, and the code is now
in a folder under the development repo. We do this so we have all parts of the
application (frontend and backend) in one repository while still having individual
git repos for the separate components of the application.

Unfortunately because of the way volumes work you will need to install the dependencies
of these applications locally. There is a work around found [here](http://jdlm.info/articles/2016/03/06/lessons-building-node-app-docker.html#the-nodemodules-volume-trick) But this hides the node_modules folder locally, so typescript wont work
in your editor. I recommend doing the following (requires npm and composer)

```bash
cd lunchrocket-frontend
npm install
npm run typings install
```

and

```bash
cd lunchrocket-backend
composer install
```

The development repo has a file called `docker-compose.yml`. This file has
instructions on how to compose the application and build all the necessary
components.

Running:
```bash
docker-compose up
```
Will spin up the entire application and give you logs for each component.


From here you should be able to visit `lunchrocket.docker` to see the application.
The docker compose file tells it to run a development server so you can make changes
to the application and they will be reflected on that page.

## CI details and Gitflow

The project uses [Gitflow](https://ja.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow/)
for it's git workflow. With that being said the GitlabCI will test and build to staging
on every commit to a release branch and will test and build to production with every
tagged commit.

At a high level the build process is that it builds a new Docker image tags it
with the appropriate version number and uploads that to Skyrockets Docker Registry
From there it sends commands to AWS Elastic Beanstalk to pull the image that was
uploaded to the Docker Registry.
